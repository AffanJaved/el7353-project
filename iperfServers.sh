#!/bin/bash

## Run iperf servers on all the APs in our topology ##
declare -a APs=("18-1" "14-10" "10-4" "17-20" "7-20" "8-3" "1-3" "4-11" "1-13" "3-19")

for i in "${APs[@]}"
do
	ssh -o "StrictHostKeyChecking no" root@node"$i" "nohup iperf -s > iperf.out 2> iperf.err < /dev/null &" 
done