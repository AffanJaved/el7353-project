GROUP=node7-1.grid.orbit-lab.org,node7-11.grid.orbit-lab.org,node9-20.grid.orbit-lab.org,node10-1.grid.orbit-lab.org,node7-14.grid.orbit-lab.org,node7-20.grid.orbit-lab.org,node1-10.grid.orbit-lab.org,node1-11.grid.orbit-lab.org,node3-1.grid.orbit-lab.org,node3-13.grid.orbit-lab.org,node1-13.grid.orbit-lab.org,node1-15.grid.orbit-lab.org,node1-19.grid.orbit-lab.org,node1-20.grid.orbit-lab.org,node3-18.grid.orbit-lab.org,node4-3.grid.orbit-lab.org,node8-1.grid.orbit-lab.org,node8-3.grid.orbit-lab.org,node1-6.grid.orbit-lab.org,node1-7.grid.orbit-lab.org,node17-20.grid.orbit-lab.org,node18-3.grid.orbit-lab.org,node18-13.grid.orbit-lab.org,node18-20.grid.orbit-lab.org,node6-20.grid.orbit-lab.org,node9-1.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node1-9.grid.orbit-lab.org,node2-19.grid.orbit-lab.org,node1-12.grid.orbit-lab.org,node1-18.grid.orbit-lab.org,node2-2.grid.orbit-lab.org,node7-7.grid.orbit-lab.org,node1-4.grid.orbit-lab.org,node17-10.grid.orbit-lab.org,node18-1.grid.orbit-lab.org

if [[ -n "$GROUP" ]]  
then  
  omf tell -a reboot -t "$GROUP"
else  
  echo "Please set the GROUP variable"
fi  

