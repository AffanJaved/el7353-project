#!/bin/bash

## Retrieve iperf log files from all clients ##
declare -a Clients=("7-1" "7-11" "9-20" "10-1" "7-14" "7-20" "1-10" "1-11" "3-1" "3-13" "1-13"$

for i in "${Clients[@]}"
do
        scp root@node"$i":iperf"$i".out .
done

for i in "${Clients[@]}"
do
        head -n -1 iperf"$i".out | grep -Po '[0-9.]*(?= Mbits/sec)' > log.txt
        declare -a myarray

        count=1
        while read p; do
                myarray[$count]=$p
            count=$((count+1))
        done < log.txt
        echo "${myarray[@]}" >> final_log.txt
        rm iperf"$i".out
done

