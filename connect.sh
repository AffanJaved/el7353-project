#!/bin/bash

#usage  connect.sh AP1
# where AP1 is the ESSID of the AP you want to connect to

modprobe ath9k
ifconfig wlan0 up
wpa_passphrase $1 nyu12345 > wpa.conf
wpa_supplicant -iwlan0 -cwpa.conf -B
dhclient wlan0