
#!/bin/bash

#load omf image on nodes in our topology

GROUP=node1-1.grid.orbit-lab.org,node1-2.grid.orbit-lab.org,node1-3.grid.orbit-lab.org,node1-4.grid.orbit-lab.org,node1-6.grid.orbit-lab.org,node1-7.grid.orbit-lab.org,node1-9.grid.orbit-lab.org,node1-10.grid.orbit-lab.org,node1-11.grid.orbit-lab.org,node1-12.grid.orbit-lab.org,node1-13.grid.orbit-lab.org,node1-15.grid.orbit-lab.org,node1-16.grid.orbit-lab.org,node1-17.grid.orbit-lab.org,node1-18.grid.orbit-lab.org,node1-19.grid.orbit-lab.org,node1-20.grid.orbit-lab.org,node2-1.grid.orbit-lab.org,node2-2.grid.orbit-lab.org,node2-19.grid.orbit-lab.org,node2-20.grid.orbit-lab.org,node3-1.grid.orbit-lab.org,node3-2.grid.orbit-lab.org,node3-13.grid.orbit-lab.org,node3-18.grid.orbit-lab.org,node3-19.grid.orbit-lab.org,node3-20.grid.orbit-lab.org,node4-3.grid.orbit-lab.org,node4-11.grid.orbit-lab.org,node4-19.grid.orbit-lab.org,node5-5.grid.orbit-lab.org,node5-16.grid.orbit-lab.org,node6-6.grid.orbit-lab.org,node6-20.grid.orbit-lab.org,node7-1.grid.orbit-lab.org,node7-7.grid.orbit-lab.org,node7-10.grid.orbit-lab.org,node7-11.grid.orbit-lab.org,node7-14.grid.orbit-lab.org,node7-20.grid.orbit-lab.org,node8-1.grid.orbit-lab.org,node8-3.grid.orbit-lab.org,node8-7.grid.orbit-lab.org,node8-8.grid.orbit-lab.org,node8-13.grid.orbit-lab.org,node8-18.grid.orbit-lab.org,node8-20.grid.orbit-lab.org,node9-1.grid.orbit-lab.org,node9-20.grid.orbit-lab.org,node10-1.grid.orbit-lab.org,node10-4.grid.orbit-lab.org,node10-7.grid.orbit-lab.org,node10-11.grid.orbit-lab.org,node10-17.grid.orbit-lab.org,node10-20.grid.orbit-lab.org,node11-1.grid.orbit-lab.org,node11-4.grid.orbit-lab.org,node11-17.grid.orbit-lab.org,node11-20.grid.orbit-lab.org,node13-3.grid.orbit-lab.org,node13-7.grid.orbit-lab.org,node13-8.grid.orbit-lab.org,node13-13.grid.orbit-lab.org,node13-14.grid.orbit-lab.org,node13-20.grid.orbit-lab.org,node14-7.grid.orbit-lab.org,node14-10.grid.orbit-lab.org,node14-11.grid.orbit-lab.org,node14-20.grid.orbit-lab.org,node16-5.grid.orbit-lab.org,node16-16.grid.orbit-lab.org,node16-20.grid.orbit-lab.org,node17-2.grid.orbit-lab.org,node17-10.grid.orbit-lab.org,node17-20.grid.orbit-lab.org,node18-1.grid.orbit-lab.org,node18-2.grid.orbit-lab.org,node18-3.grid.orbit-lab.org,node18-13.grid.orbit-lab.org,node18-19.grid.orbit-lab.org,node18-20.grid.orbit-lab.org,node19-1.grid.orbit-lab.org,node19-2.grid.orbit-lab.org,node19-3.grid.orbit-lab.org,node19-4.grid.orbit-lab.org,node19-19.grid.orbit-lab.org,node20-1.grid.orbit-lab.org,node20-2.grid.orbit-lab.org,node20-4.grid.orbit-lab.org,node20-5.grid.orbit-lab.org,node20-7.grid.orbit-lab.org,node20-8.grid.orbit-lab.org,node20-10.grid.orbit-lab.org,node20-11.grid.orbit-lab.org,node20-12.grid.orbit-lab.org,node20-16.grid.orbit-lab.org,node20-17.grid.orbit-lab.org,node20-18.grid.orbit-lab.org,node20-19.grid.orbit-lab.org

if [[ -n "$GROUP" ]]  
then  
  omf load -i baseline.ndz -t "$GROUP"
else  
  echo "Please set the GROUP variable"
fi  


if [[ -n "$GROUP" ]]  
then  
  omf tell -a on -t "$GROUP"
else  
  echo "Please set the GROUP variable"
fi  