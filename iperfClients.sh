#!/bin/bash

## Run iperf clients on all the APs in our topology and save output to log files ##
declare -a Clients=("20-2" "16-5" "14-7" "14-11" "10-1" "11-4" "14-20" "20-19" "8-20" "6-20" "8-1" "3-1" "1-2" "1-4" "7-11" "3-13" "1-12" "1-15" "3-20" "1-19")

for i in "${Clients[@]}"
do
	ssh -o "StrictHostKeyChecking no" root@node"$i" "sh -c 'cd ~/"$STR"/;nohup iperf -c 192.168.12.1 -u -b 1000M -f m -t 600 -i 120 > iperf"$i".out 2> iperf.err < /dev/null &'";
done
