#!/usr/bin/ruby
# coding: utf-8

# author : Marc Quinton, march 2013, licence : http://fr.wikipedia.org/wiki/WTFPL
#puts "start"
wait_time=1+rand(5)
sleep wait_time
#puts "was waiting"
require 'pp'

=begin
  Cell 52 - Address: 00:24:D4:51:53:20
			ESSID:"Freebox-A7D027"
			Mode:Master
			Frequency:2.427 GHz (Channel 4)
			Quality=9/70  Signal level=-86 dBm  Noise level=-95 dBm
			Encryption key:on
			Bit Rates:1 Mb/s; 2 Mb/s; 5.5 Mb/s; 11 Mb/s; 6 Mb/s
					  9 Mb/s; 12 Mb/s; 18 Mb/s; 24 Mb/s; 36 Mb/s
					  48 Mb/s; 54 Mb/s
			Extra:bcn_int=96
WPA1
		IE: WPA Version 1
			Group Cipher : CCMP
			Pairwise Ciphers (1) : CCMP
			Authentication Suites (1) : PSK
		Extra:wme_ie=dd180050f2020101000403a4000027a4000042435e0062322f00
WPA2
		IE: IEEE 802.11i/WPA2 Version 1
		Group Cipher : CCMP
		Pairwise Ciphers (1) : CCMP
		Authentication Suites (1) : PSK    
    
=end

class Cell
	attr_reader :ssid, :address, :mode, :encryption
	def initialize lines
		@values={}
		# @lines = lines.split("\n")
		lines.split("\n").each do |s|
			if m=s.match(/ESSID:"(.*)"/)
				@values[:ssid] = m[1]
			elsif m=s.match(/Mode:(.*)/)
				@values[:mode] = m[1]
			elsif m=s.match(/Frequency:(.*?) \(Channel (\d+)\)/)
				@values[:frequency] = m[1] ; @values[:channel]=m[2].to_i
			elsif m=s.match(/Address: (.*)/)
				@values[:address] = m[1]
			elsif m=s.match(/Encryption key:(.*)/)
				@values[:encryption] = m[1]
				if @values[:encryption] == 'on'
					@values[:wpa] = 'wep'
				else
					@values[:wpa] = '-'
				end
			elsif m=s.match(/WPA Version (.*)/)
				@values[:wpa] = 'wpa1'
			elsif m=s.match(/WPA2/)
				@values[:wpa] = 'wpa2'
			# Quality=9/70  Signal level=-86 dBm  Noise level=-95 dBm
			elsif m=s.match(/Quality=(.*?) .*Signal level=-*(\d+) dBm  Noise level=(.*) dBm/)
				@values[:quality] = self.quality_to_percent(m[1])
				@values[:level] = m[2]
				@values[:noise] = m[3]
			end

		end
	end
	def quality_to_percent(q)
		n = q.split("/").map(&:to_i)
		return (n[0] * 100) / n[1]
	end

	def to_csv
		keys = [:ssid, :mode, :channel, :address, :quality,:level,:noise,:wpa,:encryption]
		if @values.size == 0
			l=[]
			keys.each { |k| l<< k.to_s}
			return l.join(';')
		end
		list=[]
		keys.each do |key|
			list << @values[key]
		end
		return list.join(';')
	end

	def compare_channel (string)
			my_channel=@values[:channel]
			if my_channel	 == string
				return 1
			else
				return 0
			end		
	end

	def compare_ssid (string)
			my_ssid=@values[:ssid]
			if my_ssid == string
				return 1
			else
				return 0
			end		
	end

	def get_channel 
			return @values[:channel].to_i
	end

end

class IwlistScanParser

	def initialize
	end
	
	def parse content
		cells = []
		content.split(/Cell /m).each do |txt|
			cells << Cell.new(txt)
		end
		
		return cells
	end
end

def mda(width,height)		#creates an empty 2D array and returns it
		a = Array.new(width,0)
		a.map! { Array.new(height,0) }
		return a
end

parser = IwlistScanParser.new
cells = parser.parse ARGF.read

##below this is my code for computing interference matrix at the client
interference_value=[1,0.7272,0.2714,0.0375,0.0054,0]	#each index shows the value of interferfence for corresponding channel separation
conflict_vector=Array.new(11,0);		#shows the conflict i am experiencing on each channel
ap_IDS=["AP1","AP2","AP3","AP4","AP5","AP6","AP7","AP8","AP9","AP10","AP11","AP12","AP13","AP14","AP15","AP16","AP17","AP18","AP19","AP20"]	#each client needs to know the IDs of all the APs

cells.each do |cell|
	#puts cell.to_csv 		#this prints out the iwlist scan output
	for i in 1..11
		ap_IDS.each do |ap_ID|
			if cell.compare_ssid(ap_ID) == 1 #means i can hear this AP in scan operation
				if cell.compare_channel(i) == 1 #means I can hear this AP in scan operation
					#puts cell.get_channel
					$itera=0;
					for j in 0..5
						ind=i+j
						ind=ind.to_i
						ind=ind-1
				
						ind2=i-j
						ind2=ind2.to_i
						ind2=ind2-1
						if ind <= 10
							conflict_vector[ind]=conflict_vector[ind]+interference_value[j];
						end
						if ind2 >=0 && ind2!=ind
							conflict_vector[ind2]=conflict_vector[ind2]+interference_value[j];
						end
					end
				end
			end
		end
	end
	#puts "My Interference Vector: #{conflict_vector}"	
end

channel_assigned=conflict_vector.each_with_index.min[1]+1
#puts "Channel Assigned: Channel #{channel_assigned}"
#puts "Minimum Conflict: #{conflict_vector.each_with_index.min[0]}"

puts "#{channel_assigned}"
