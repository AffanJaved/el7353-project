#!/bin/bash

#we declare the APs and Clients according to their serial number. So node18-19 is AP1, node1-20 is AP2 and so on
#similarly, node 19-19 is Client1, node2-20 is Client2 and so on.

declare -a APs=("3-19" "13-20" "13-7" "1-6" "1-13" "8-13" "20-10" "4-19" "10-1" "2-2")
declare -a Clients=("3-20" "4-3" "14-7" "14-10" "13-8" "13-13" "1-19" "1-20" "1-15" "1-16" "8-18" "8-20" "20-12" "20-19" "5-5" "5-16" "10-4" "10-7" "2-19" "3-1")
count=1
APnum=1
for i in "${Clients[@]}"
do
        ssh root@node"$i" 'bash -s' -- < connect.sh AP"$APnum"
        echo "Connecting Client$count to AP$APnum"
        if [ $((count%2)) -eq 0 ]
        then
                APnum=$((APnum+1))
        fi
        count=$((count+1))
done



